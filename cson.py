# coding: utf8

# cson (schema-compessed json)
# author: Ondrej Sika, http://ondrejsika.com, ondrej@ondrejsika.com


__all__ = ("loads", )


def is_cson(data):
    return type(data) == list and len(data) > 0 and data[0] == "__cson"

def base_csono2jsono(data):
    if is_cson(data):
        json_data = []
        i = 0
        for element in data[2:]:
            json_data.append({})
            for k, v in zip(data[1], element):
                json_data[i][k] = v
            i += 1
        return json_data

def cson2json(cson_data):
    def loop(cson_data):
        if type(cson_data) == list:
            iterable = range(len(cson_data))
        elif type(cson_data) == dict:
            iterable = cson_data.keys()
        for i in iterable:
            if type(cson_data[i]) == tuple:
                cson_data[i] = list(cson_data[i])
            if is_cson(cson_data[i]):
                cson_data[i] = base_csono2jsono(cson_data[i])
                loop(cson_data[i])
            elif type(cson_data[i]) in (list, dict):
                loop(cson_data[i])
    if type(cson_data) == tuple:
        cson_data = list(cson_data)
    if is_cson(cson_data):
        cson_data = base_csono2jsono(cson_data)
        loop(cson_data)
    elif type(cson_data) in (list, dict):
        loop(cson_data)
    return cson_data


loads = cson2json