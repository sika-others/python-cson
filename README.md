python-cson
===========

Python library for CSON (schema-compressed JSON)


Authors
-------
*  Ondrej Sika, <http://ondrejsika.com>, dev@ondrejsika.com

Source
------
* Documentation: <http://ondrejsika.com/docs/python-cson>
* Python Package Index: <http://pypi.python.org/pypi/cson>
* GitHub: <https://github.com/sikaondrej/python-cson>
