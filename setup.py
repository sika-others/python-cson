#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "cson",
    version = "1.0.0",
    url = 'http://ondrejsika.com/docs/python-cson/',
    download_url = 'https://github.com/sikaondrej/python-cson/',
    license = 'MIT',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    py_modules = ["cson"],
    include_package_data = True,
    zip_safe = False,
)
